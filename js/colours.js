/*
Draw something app ver 1
Author: Luke Davies
* Copyright 2015 Luke Davies
*/

var colours = ['black', 'grey', 'white', 'red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet'];

 for(var i=0, n=colours.length; i<n;i++) {
 	var colour = document.createElement('div');
 	colour.className = 'colour';
 	colour.style.backgroundColor = colours[i];
 	colour.addEventListener('click', setColour);
 	document.getElementById('colour-pallet').appendChild(colour);
 }

 function setColor(color) {
 	context.fillStyle = color;
 	context.strokeStyle = color;
 	var active = document.getElementsByClassName('active')[0];
 	 if(active) {
 	 	active.className = 'colour';
 	 }
 }

 function setColour(e) {
 	//identify swatch
 	var colour = e.target;
 	//set color
 	setColor(colour.style.backgroundColor);
 	//give active class
 	colour.className += ' active';
 }

 setColour({ target: document.getElementsByClassName('colour')[0]});